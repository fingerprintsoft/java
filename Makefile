MAJOR ?= 8
MINOR ?= 92
BUILD ?= 14
.PHONY: all

all: build push

build:
	docker build --rm -t fingerprintsoft/java .
	docker tag fingerprintsoft/java fingerprintsoft/java:${MAJOR}.${MINOR}.${BUILD}

push:
	docker push fingerprintsoft/java
	docker push fingerprintsoft/java:${MAJOR}.${MINOR}.${BUILD}

